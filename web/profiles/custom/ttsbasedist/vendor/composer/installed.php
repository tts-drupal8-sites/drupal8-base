<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'drupal-custom-profile',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '6a094967f92c0a15a667878f794f50aa61995fc1',
        'name' => 'tcas/ttsbasedist',
        'dev' => true,
    ),
    'versions' => array(
        'tcas/ttsbasedist' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'drupal-custom-profile',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '6a094967f92c0a15a667878f794f50aa61995fc1',
            'dev_requirement' => false,
        ),
    ),
);
