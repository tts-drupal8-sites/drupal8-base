#!/bin/bash

     srcsite="/srv/www/drupal/web/sites/transformativeideas.duke.edu"
     trgsite="/srv/www/drupal/web/sites/documentarystudies.duke.edu"
   srcd8site="transformativeideas.duke.edu"
   trgd8site="documentarystudies.duke.edu"
srcstgd8site="transformativeideas-staging.trinity.duke.edu"
trgstgd8site="documentarystudies-staging.trinity.duke.edu"

# Common

cp $srcsite/config/prod/system.performance.yml \
   $trgsite/config/prod/system.performance.yml

# Prod
cp $srcsite/config/prod/purge.logger_channels.yml \
   $trgsite/config/prod/purge.logger_channels.yml

cp $srcsite/config/prod/purge.plugins.yml  \
   $trgsite/config/prod/purge.plugins.yml 

cp $srcsite/config/prod/purge_queuer_coretags.settings.yml  \
   $trgsite/config/prod/purge_queuer_coretags.settings.yml 

cp $srcsite/config/prod/varnish_purger.settings.972faa81be.yml  \
   $trgsite/config/prod/varnish_purger.settings.972faa81be.yml 

sed -i "s/$srcd8site/$trgd8site/g" \
   $trgsite/config/prod/varnish_purger.settings.972faa81be.yml
#read -p "Check config/prod/ files"

# Stage
cp $srcsite/config/stage/system.performance.yml \
   $trgsite/config/stage/system.performance.yml

cp $srcsite/config/stage/purge.logger_channels.yml \
   $trgsite/config/stage/purge.logger_channels.yml

cp $srcsite/config/stage/purge.plugins.yml  \
   $trgsite/config/stage/purge.plugins.yml 

cp $srcsite/config/stage/purge_queuer_coretags.settings.yml  \
   $trgsite/config/stage/purge_queuer_coretags.settings.yml 

cp $srcsite/config/stage/varnish_purger.settings.972faa81be.yml  \
   $trgsite/config/stage/varnish_purger.settings.972faa81be.yml 

sed -i "s/$srcstgd8site/$trgstgd8site/g" \
   $trgsite/config/stage/varnish_purger.settings.972faa81be.yml
#read -p "Check config/stage/ files"

# Config Split

# prod
vimdiff -o  $trgsite/config/sync/config_split.config_split.prod.yml \
   $srcsite/config/sync/config_split.config_split.prod.yml

# stage
vimdiff -o $trgsite/config/sync/config_split.config_split.stage.yml \
   $srcsite/config/sync/config_split.config_split.stage.yml

#read -p "Check config/sync/ config_split.config files"

## core.extension.yml
#cp $srcsite/config/sync/core.extension.yml \
#   $trgsite/config/sync/core.extension.yml
##read -p "Check config/sync/ core.extension files"

sed -i 's/venus_triad/tts_drupal_01/' $trgsite/.gitlab-ci.yml
vim $trgsite/.gitlab-ci.yml

cd /$trgsite/
git diff origin/staging -- ./config/sync/

