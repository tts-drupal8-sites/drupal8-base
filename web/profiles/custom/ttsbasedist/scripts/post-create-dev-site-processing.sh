#!/bin/bash

if [ $# -ne 3 ]
then
  echo "==> please pass the drupalroot, site description and drupalsite  as run-time parameters (ex. '/srv/www/drupal' 'sociology' 'sociology.duke.edu"
  exit
fi

drupalroot=$1
site_desc=$2
drupalsite=$3
shortsitename=`echo "$drupalsite" | grep -o '^[^\.]*'`
mydir=`pwd`
#read -p  "==> drupalroot: $drupalroot, site_desc: $site_desc, drupalsite: $drupalsite"
# Make sure all critical folders exist
if [ ! -d $drupalroot ]
then
  echo "==> DrupalRoot: $drupalroot, does not exist"
  exit
fi

if [ ! -d $drupalroot/web/sites/$drupalsite ]
then
  echo "==> DrupalSite: $drupalroot/web/sites/$drupalsite, does not exist"
  exit
fi

echo "==> cd to site folder"
cd $drupalroot/web/sites/$drupalsite

if [ ! -d files/background_image/css ]
then
  mkdir -p files/background_image/css
fi

ls -la
#read -p "..."

echo "==> run cr and permissions script"
drush cr; sudo /usr/local/etc/drupal_permissions.sh $drupalroot $drupalsite;
#read -p "..."

echo "==> run node_access_rebuild" 
drush php-eval 'node_access_rebuild();'
#read -p "..."
echo "==> GUI: run any pending search_api tasks"

echo "==> run drush cron"
drush cron
#read -p "..."

# import optional
echo "==> importing optional config"
drush cr; drush cim --partial --source=/srv/www/drupal/web/profiles/custom/ttsbasedist/config/optional/ -y; drush cr
drush cex -y
read -p "..."

# import tmp config
echo "==> enabling additional modules"
drush en media_entity_browser -y
drush en redirect -y
drush en redirect_404 -y
read -p "..."

echo "==> importing tmp config"
rsync -av --no-g --no-times --delete $drupalroot/web/profiles/custom/ttsbasedist/config/tmp/ config/tmp/
drush cr; drush cim --partial --source=sites/$drupalsite/config/tmp/ -y ; drush cr
drush cex -y
read -p "..."

# import tmp2 config
echo "==> importing tmp2 (basicshib) config"
rsync -av --no-g --no-times --delete $drupalroot/web/profiles/custom/ttsbasedist/config/tmp2/ config/tmp/
drush cr; drush cim --partial --source=sites/$drupalsite/config/tmp/ -y ; drush cr
read -p "..."

echo "==> export config"
php -d memory-limit=-1 /usr/local/sbin/drush cex -y
#read -p "..."

echo "==> remove GUIDs"
find ./config/ -type f -exec sed -i -e '/_core:/,+1d' {} \;
#read -p "..."

echo "==> import and export config"
php -d memory-limit=-1 /usr/local/sbin/drush cim -y
php -d memory-limit=-1 /usr/local/sbin/drush cex -y
#read -p "..."

echo "==> setup config split"
cd $drupalroot/web/profiles/custom/ttsbasedist/scripts/config-split-setup/
./setup-config-split.sh $drupalroot "$site_desc" $drupalsite
#read -p "..."

echo "==> check the settings.local.php environment and optionally setup the migration section"
cd $drupalroot/web/sites/$drupalsite
sed -i "s/^\/\/\$config\['config_split\.config_split\.dev/\$config\['config_split\.config_split\.dev/"  settings.local.php
#vim settings.local.php
#read -p "..."

read -p "==> any key to continue with cim"
echo "==> cim"
php -d memory-limit=-1 /usr/local/sbin/drush cim -y
read -p "..."

#read -p "==> any key to continue with cex"
echo "==> cex"

php -d memory-limit=-1 /usr/local/sbin/drush cex -y
#read -p "==> any key to coninue with commiting config"
echo "==> commit config"

echo "==> cd to site folder"
cd $drupalroot/web/sites/$drupalsite
ls -la
#read -p "..."

#read -p "==> any key to continue with git add/commit"
echo "==> git add/commit"
git add --all
git commit -m "Commit config after creating dev site"
#read -p "===> any key to push to gitlab"
echo "==> push to gitlab"
git push origin dev
echo "==> create users"
cd $drupalroot/web/profiles/custom/ttsbasedist/scripts/
./create_users.sh $drupalroot $drupalsite
