#!/bin/bash

if [ $# -ne 3 ]
then
  echo "==> please pass the drupalroot, short sitename and sitename  as run-time parameters (ex. '/var/www/drupal' 'sociology' 'sociology.duke.edu"
  exit
fi

drupalroot=$1
site_desc=$2
sitename=$3
shortsitename=`echo "$sitename" | grep -o '^[^\.]*'`
config_split_setup_folder=`pwd`
mydir=`pwd`

# Make sure all critical folders exist
if [ ! -d $drupalroot ]
then
  echo "==> DrupalRoot: $drupalroot, does not exist"
  exit
fi

if [ ! -d $drupalroot/web/sites/$sitename ]
then
  echo "==> DrupalSite: $drupalroot/web/sites/$sitename, does not exist"
  exit
fi

if [ ! -d $drupalroot/web/sites/$sitename/config/sync ]; then
  mkdir -p $drupalroot/web/sites/$sitename/config/sync
fi

if [ ! -d $drupalroot/web/sites/$sitename/config/prod ]; then
  mkdir -p $drupalroot/web/sites/$sitename/config/prod
fi

if [ ! -d $drupalroot/web/sites/$sitename/config/stage ]; then
  mkdir -p $drupalroot/web/sites/$sitename/config/stage
fi

if [ ! -d $drupalroot/web/sites/$sitename/config/dev ]; then
  mkdir -p $drupalroot/web/sites/$sitename/config/dev
fi

echo "
drupalroot=$drupalroot
sitename=$sitename
shortsitename=$shortsitename
site_desc=$site_desc
config_split_setup_folder=$config_split_setup_folder
"
#read -p "..."
# Prep the config-split files
cd $config_split_setup_folder

# Do all work in temp area
rsync -a --delete config/ tmp/
echo " "

#        uuid='9be59459-5f0b-475a-a5ae-7c121decb215'
template_uuid='TEMPLATEUUID'
#        sitename="d8-demo.trinity.duke.edu"
template_sitename="TEMPLATESITENAME"
#        shortsitename="9191919"
template_shortsitename="TEMPLATESHORTSITENAME"
#        site_desc="D8 Demo"
template_site_desc="TEMPLATESITEDESCRIPTION"

cd $drupalroot/web/sites/$sitename
/usr/local/sbin/drush pmu google_analytics
/usr/local/sbin/drush en config_split
cd $mydir

#read -p "..."

# Spit out parameters
# Extract site UUID
cd $drupalroot/web/sites/$sitename
uuid=`drush cget "system.site" uuid | grep 'system.site:uuid' | grep -o '[^ ]*$'`
echo "d8-demo UUID: $template_uuid"
echo "        UUID: $uuid
"
cd $config_split_setup_folder

echo "==> Updating UUIDS"
find tmp/ -type f -exec sed -i "s/$template_uuid/$uuid/g" {} \;
grep -RH 'uuid' tmp/*
echo " "

echo "==> Updating $template_sitename to $sitename"
find tmp/ -type f -exec sed -i "s/$template_sitename/$sitename/g" {} \;
grep -RH "$template_sitename\|$sitename" tmp/*
echo " "

echo "==> Updating $template_shortsitename to $shortsitename"
find tmp/ -type f -exec sed -i "s/$template_shortsitename/$shortsitename/g" {} \;
grep -RH "$template_shortsitename\|$shortsitename" tmp/*
echo " "

echo "==> Updating $template_site_desc to $site_desc"
find tmp/ -type f -exec sed -i "s/$template_site_desc/$site_desc/g" {} \;
grep -RH "$template_site_desc\|$site_desc" tmp/*
echo " "

#read -p "..."

# Export current site config
cd $drupalroot/web/sites/$sitename
drush cex -y
echo " "

# Rsync config-split files into place
rsync -av "$config_split_setup_folder/tmp/" "$drupalroot/web/sites/$sitename/config/"

# Remove files from config/sync
rm -f \
$drupalroot/web/sites/$sitename/config/sync/devel.settings.yml \
$drupalroot/web/sites/$sitename/config/sync/devel.toolbar.settings.yml \
$drupalroot/web/sites/$sitename/config/sync/field_ui.settings.yml \
$drupalroot/web/sites/$sitename/config/sync/stage_file_proxy.settings.yml \
$drupalroot/web/sites/$sitename/config/sync/syslog.settings.yml \
$drupalroot/web/sites/$sitename/config/sync/system.logging.yml \
$drupalroot/web/sites/$sitename/config/sync/system.menu.devel.yml \
$drupalroot/web/sites/$sitename/config/sync/system.performance.yml \
#$drupalroot/web/sites/$sitename/config/sync/system.site.yml

# Rebuild cache
cd $drupalroot/web/sites/$sitename
drush cr

rm -f $drupalroot/web/sites/$sitename/config/tmp/*
#read -p "..."
rsync -av --delete config/sync/config_split* config/tmp/

cd $drupalroot/web/sites/$sitename/

#read -p "==> go forward with: drush cr; drush cim --partial --source=sites/$sitename/config/tmp/ ; drush cr"

drush cr; drush cim --partial --source=sites/$sitename/config/tmp/ -y ; drush cr
echo "
==> 1. Update the settings.local.php file for the correct environment
    2. Import the entire config

	drush cim; drush cr

    3. Rebuild the Drupal cache
"

echo "
==> Performing config changes for you
"
