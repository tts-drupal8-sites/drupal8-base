#!/bin/bash

mydir=`pwd`
echo "working directory: $mydir
"

fldr="/srv/www/drupal/web/sites/polisci.duke.edu/config"
rsync -av --delete tcas-drupal-dev-02.oit.duke.edu:${fldr}/ ~/tmp-config/
fltr="~/tmp-config"

if [ $# -eq 1 ]
then
  fldr=$1
fi

if [ ! -d "$fldr" ]
then
  echo "Missing base site config folder: $fldr"
  exit
fi

echo "Process files in sync:
"
cd $fldr/sync
for fn in `find . -type f -name 'config_split*' | grep 'dev\|stage\|^prod$' | sort`
do
  fn=`echo $fn | sed -e 's/^\.\///'`
  read -p "==> Processing: $fn"
  vimdiff -o "$mydir/config/sync/$fn" "$fldr/sync/$fn"
done

cd $fldr/sync
for fn in `find . -type f | grep 'core\.extension\.yml\|system\.site\.yml' | sort`
do
  fn=`echo $fn | sed -e 's/^\.\///'`
  read -p "==> Processing: $fn"
  vimdiff -o "$mydir/config/sync/$fn" "$fldr/sync/$fn"
done


echo "
Process files in dev:
"
cd $fldr/dev
for fn in `find . -type f | sort`
do
  fn=`echo $fn | sed -e 's/^\.\///'`
  read -p "==> Processing: $fn"
  vimdiff -o "$mydir/config/dev/$fn" "$fldr/dev/$fn"
done

echo "
Process files in stage:
"
cd $fldr/stage
for fn in `find . -type f | sort`
do
  fn=`echo $fn | sed -e 's/^\.\///'`
  read -p "==> Processing: $fn"
  vimdiff -o "$mydir/config/stage/$fn" "$fldr/stage/$fn"
done

echo "
Process files in prod:
"
cd $fldr/prod
for fn in `find . -type f | sort`
do
  fn=`echo $fn | sed -e 's/^\.\///'`
  read -p "==> Processing: $fn"
  vimdiff -o "$mydir/config/prod/$fn" "$fldr/prod/$fn"
done
cd $mydir
