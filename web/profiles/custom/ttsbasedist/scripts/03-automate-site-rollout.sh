#!/bin/bash

if [ $# -ne 3 ]
then
  echo "
Script requires the following parameters
  drupalroot: (/srv/www/drupal)
  site_desc:   (ex.' Sociology Department')
  drupalsite: (ex. sociology.duke.edu)
"
  exit
fi
drupalroot=$1
site_desc=$2
drupalsite=$3
if [ -d "$drupalroot/web" ]
then
  webroot="$drupalroot/web"
else
  webroot="$drupalroot"
fi
siteroot="$webroot/sites"

#echo "
#site_desc:  $site_desc
#drupalsite: $drupalsite
#drupalroot: $drupalroot
#webroot:    $webroot
#siteroot:   $siteroot
#"

# NOTE: all updates of the install profile should be down before running this script

# Missing code here for removing the default image (see additional...sh)
cd $webroot/profiles/custom/ttsbasedist
./scripts/site-install.sh $drupalroot "$site_desc" $drupalsite
read -p "...any key to continue..."
./scripts/post-create-dev-site-processing.sh $drupalroot "$site_desc" $drupalsite
