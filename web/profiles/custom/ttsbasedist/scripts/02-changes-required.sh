#!/bin/bash

mydir=''
drupalroot="/srv/www/drupal"
webroot="$drupalroot/web"
profilename="ttsbasedist"
profileroot="$webroot/profiles/custom/$profilename"
date_time=`date +"%Y%m%d-%HH%MM"`
rslt=""

if [ -f ~/temp.txt ]
then
 rm -f ~/temp.txt
fi

if [ ! -f TTSBaseDist.info.yml ]
then
  echo "==> Script must be run from the profile directory 'ttsbasedist'"
  exit
fi
mydir=`pwd`

# Core Modules
ssh awssmith@tcas-drupal-dev-02.oit.duke.edu "drush -l d8-ref-dev.trinity.duke.edu pml --core --status=enabled | grep Core |grep -v 'classy\|seven\|stable\|Experimental\|claro' | grep -o '([^)]*' | sed -e 's/(//g' | sort | sed -e 's/^/  - /'" > ~/temp.txt

# Non-core Module
ssh awssmith@tcas-drupal-dev-02.oit.duke.edu "drush -l d8-ref-dev.trinity.duke.edu pml --no-core --status=enabled |  grep -o '([^)]*' | sed -e 's/(//g' | grep -v 'CSV\|tts_base\|tts_labs_ctrs\|tts_sub\|gin$\|bootstrap_barrio\|^contrib$' | sort | sed -e 's/^/  - /'">> ~/temp.txt

read -p "==> Are there updates to the core/non-core modules?"
vimdiff -o TTSBaseDist.info.yml ~/temp.txt

read -p "==> Updates to core.extenstions.yml file for config-split"
vimdiff -o scripts/config-split-setup/config/sync/core.extension.yml ~/d8-ref-dev_trinity_duke_edu/config/sync/core.extension.yml
