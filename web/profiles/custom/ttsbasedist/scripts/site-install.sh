#!/bin/bash

date_time=`date +"%Y%m%d-%HH%MM"`
start_time=$date_time
drush=`which drush`

if [ $# -ne 3 ]
then
  echo "
Script requires the following parameters
  drupalroot: (/srv/www/drupal)
  sitename:   (ex. Sociology)
  drupalsite: (ex. sociology.duke.edu)
"
  exit
fi
drupalroot=$1
sitename=$2
drupalsite=$3
if [ -d "$drupalroot/web" ]
then
  webroot="$drupalroot/web"
else
  webroot="$drupalroot"
fi
siteroot="$webroot/sites"

echo "
sitename:   $sitename
drupalsite: $drupalsite
drupalroot: $drupalroot
webroot:    $webroot
siteroot:   $siteroot
"

if [ ! -d "$siteroot/$drupalsite" ]
then
  echo "
Error: drupal site does not exist: $siteroot/$drupalsite"
  exit
fi

rm -rf $siteroot/$drupalsite/config/
mkdir -p $siteroot/$drupalsite/config/sync

read -p "==> 
Enter the name of the file containing DB credentials (settings.local.php(default), settings.php or settings.ddev.php
)" settings_file
if [ "$settings_file" == "" ]
then
  settings_file="settings.local.php"
fi
if [ "$settings_file" == "settings.ddev.php" ]
then
  db_database='db'
  db_hostname='db'
  db_username='db'
  db_password='db'
else
  db_database=`cat "$siteroot/$drupalsite/$settings_file" | grep -v '^\/\|[ \t]*\*' | sed -e 's/[ \t][ \t]*/ /g' | grep -Po "(?<='database' => ')[^',]+"`
  db_hostname=`cat "$siteroot/$drupalsite/$settings_file" | grep -v '^\/\|[ \t]*\*' | sed -e 's/[ \t][ \t]*/ /g' | grep -Po "(?<='host' => ')[^',]+"`
  db_username=`cat "$siteroot/$drupalsite/$settings_file" | grep -v '^\/\|[ \t]*\*' | sed -e 's/[ \t][ \t]*/ /g' | grep -Po "(?<='username' => ')[^',]+"`
  db_password=`cat "$siteroot/$drupalsite/$settings_file" | grep -m1 "'password'" | sed -e 's/[ \t][ \t]*/ /g' | grep -Po "(?<='password' => ').*$"`
fi
db_password=`echo $db_password | tr -d "\'"`
db_password=`echo $db_password | sed -e 's/,$//g'`
db_password=`echo $db_password | tr -d "\""`
echo "
Run time options:
db_database:  $db_database
db_username:  $db_username
db_hostname:  $db_hostname
"
read -p "..."
cd $siteroot
$drush -vvv si \
  TTSBaseDist install_configure_form.enable_update_status_emails=NULL \
  --db-url="mysql://$db_username:$db_password@$db_hostname:3306/$db_database" \
  --account-name='tts-admin' \
  --account-mail='trinitywebsupport@duke.edu' \
  --site-mail='trinitywebsupport@duke.edu' \
  --account-pass='3n09\$!(!rE' \
  --site-name="$sitename" \
  --sites-subdir="$drupalsite" \
  -y

end_time=`date +"%Y%m%d-%HH%MM"`
echo "
==> Start Time: $start_time
      End Time: $end_time
"
