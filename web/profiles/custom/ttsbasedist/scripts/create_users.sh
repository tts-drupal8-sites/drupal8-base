#!/bin/bash

if [ $# -ne 2 ]
then
  echo "
==> Script expects 2 runtime parameters: Drupal Root and Drupal site name
    ex. ./create-users.sh /srv/www/drupal it.duke.edu"
  exit
fi

drupalroot=$1
sitename=$2

echo "
==> Drupal Root: $drupalroot
    Sitename   : $sitename"

if [ ! -d "$drupalroot" ]
then
  echo "
==> Missing drupal root: $drupalroot"
  exit
fi

if [ ! -d "$drupalroot/web/sites/$sitename" ]
then
  echo "
==> Missing site: $sitename"
  exit
fi

function createit {
  netid=$1
  role=$2

  drush ucrt $netid@duke.edu --mail=$netid@duke.edu
  drush urol $role $netid@duke.edu
}

cd $drupalroot/web/sites/$sitename
createit jbo2 'administrator'
createit awssmith 'administrator'
createit swv4 'administrator'
createit dmw60 'administrator'
createit dm313 'administrator'
createit hlw25 'site_manager'
