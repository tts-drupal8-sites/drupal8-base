#!/bin/bash

mydir=''
drupalroot="/var/www/drupal"
webroot="$drupalroot/web"
profilename="ttsbasedist"
profileroot="$webroot/profiles/custom/$profilename"
date_time=`date +"%Y%m%d-%HH%MM"`
rslt=""

function backitup {
  # backup current install profile
  rsync -av $profileroot/ ~/$profilename-$date_time/
}

function removeit {
  if [ -f $1 ]
  then
    rm -f $1
  fi
}

function copyit {
  # clone repo
  cd ~
  if [ -d d8-sb-dev_trinity_duke_edu ]
  then
    rm -rf d8-sb-dev_trinity_duke_edu
  fi
  git clone git@gitlab.oit.duke.edu:tts-drupal8-sites/d8-sb-dev_trinity_duke_edu.git
  git checkout dev

  # remove migrate configs
  cd d8-sb-dev_trinity_duke_edu
  removeit config/sync/migrate_drupal.settings.yml
  removeit config/sync/migrate_plus.migration.department_members.yml
  removeit config/sync/migrate_plus.migration_group.default.yml
  removeit config/sync/migrate_plus.migration_group.migrate_config_settings.yml
  removeit config/sync/tts_migrate_duke_sources.settings.yml
  
  cd ~
  if [ -d config/sync ]
  then
    rm -rf config/sync
  fi
  mkdir -p config/sync
  rsync -av --delete d8-sb-dev_trinity_duke_edu/config/sync/ ~/config/sync
}

function moveit {
  mv $1 $mydir/config/optional/
}

function updateit {
  # Remove the old config.
  rm $mydir/config/install/*;
  rm $mydir/config/optional/*;
  #rm $mydir/config/post-install/*;
  
  # Copy over the site's config.
  cp ~/config/sync/* $mydir/config/install/;
  
  # Jay Beaton, dependencies
  moveit "$mydir/config/install/block.block.*"

  # TCAS, depentencies
  moveit "$mydir/config/install/basicshib.*"
  moveit "$mydir/config/install/core.entity_form_*"
  moveit "$mydir/config/install/core.entity_view_*"
  moveit "$mydir/config/install/crop.*"
  moveit "$mydir/config/install/editor.editor.*"
  moveit "$mydir/config/install/embed.button.*"
  moveit "$mydir/config/install/entity_browser.browser.*"
  moveit "$mydir/config/install/entityqueue.*"
  moveit "$mydir/config/install/feeds.feed_type.*"
  moveit "$mydir/config/install/field.field.*"
  moveit "$mydir/config/install/field.storage.*"
  moveit "$mydir/config/install/file_entity.type.*"
  moveit "$mydir/config/install/filter.format.*"
  moveit "$mydir/config/install/image.*"
  moveit "$mydir/config/install/media.type.*"
  moveit "$mydir/config/install/menu_position.*"
  moveit "$mydir/config/install/metatag.metatag_defaults.*"
  moveit "$mydir/config/install/paragraphs.paragraphs_type.*"
  moveit "$mydir/config/install/pathauto.pattern.*"
  moveit "$mydir/config/install/rabbit_hole.behavior_settings.*"
  moveit "$mydir/config/install/search_api.*"
  moveit "$mydir/config/install/slick.optionset.*"
  moveit "$mydir/config/install/system.action.*"
  moveit "$mydir/config/install/tts_*"
  moveit "$mydir/config/install/user.role.site_manager.*"
  moveit "$mydir/config/install/views.view.*"
  
  # Move workflow config to post-install.
  #mv $mydir/config/install/workflows.workflow.editorial.yml $mydir/config/post-install/
  
  # Don't want the list of enabled modules.
  rm $mydir/config/install/core.extension.yml;
  #
  # Don't want devel settings.
  rm $mydir/config/install/devel.*.yml;
  rm $mydir/config/install/*.devel.yml;
  
  #
  # Remove UUIDs, etc.
  # For use on Macs:
  #find $mydir/config/ -type f -exec sed -i '' '/^uuid: /d' {} \;
  #find $mydir/config/ -type f -exec sed -i '' '/_core/{N;d;}' {} \;
  # For use on Linux:
  find $mydir/config/ -type f -exec sed -i -e '/^uuid: /d' {} \;
  find $mydir/config/ -type f -exec sed -i -e '/_core:/,+1d' {} \;
  
  # After Install
  #had to change the front page from /node/16 to /node
  #had to run the shibboleth patch script
  sed -i 's/node\/16/node/g' $mydir/config/install/system.site.yml
}

function defaultimage {
  sed -i '/default_image:/,+5 d' $mydir/config/optional/field.field.block_content.footer_block.field_footer_logo.yml
}

function prompt {
  read -p "$1
" rslt
  if [ "$rslt" == "y" ]
  then
    $2
  fi
}

if [ ! -f TTSBaseDist.info.yml ]
then
  echo "==> Script must be run from the profile directory 'ttsbasedist'"
  exit
fi
mydir=`pwd`
prompt "==> backup current install profile? (y/n)" backitup
prompt "==> copy profile from d8-sb-dev? (y/n)" copyit
prompt "==> process config dependencies? (y/n)" updateit
prompt "==> remove default image? (y/n)" defaultimage
