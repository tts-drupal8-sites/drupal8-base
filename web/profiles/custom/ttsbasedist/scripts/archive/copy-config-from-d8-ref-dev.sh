#!/bin/bash

cd ~
if [ -d d8-ref-dev_trinity_duke_edu ]
then
  rm -rf d8-ref-dev_trinity_duke_edu
fi
git clone git@gitlab.oit.duke.edu:tts-drupal8-sites/d8-ref-dev_trinity_duke_edu.git
cd d8-ref-dev_trinity_duke_edu
# remove migrate configs
#ls -l config/sync/migrate*
rm config/sync/migrate_drupal.settings.yml
rm config/sync/migrate_plus.migration.department_members.yml
rm config/sync/migrate_plus.migration_group.default.yml
rm config/sync/migrate_plus.migration_group.migrate_config_settings.yml
rm config/sync/tts_migrate_duke_sources.settings.yml
ls -l config/sync/migrate* config/sync/tts_migrate*
read -p "..."

git checkout dev
cd ~
#read -p "..."
rsync -av --delete d8-ref-dev_trinity_duke_edu/config/sync/ /var/www/drupal/config/sync
cd /var/www/drupal/web/profiles/custom/ttsbasedist/
chmod 775 ./scripts/update-install-config.sh
./scripts/update-install-config.sh
./scripts/additional-config-changes.sh

