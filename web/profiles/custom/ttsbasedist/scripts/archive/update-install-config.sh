#!/usr/bin/env bash

CONFIG_DIR="../../../../config/sync";

chmod 777 ../../../../web/core/install.php 
#chmod 664 ../../../../web/sites/*/settings*

if [ ! -f "./TTSBaseDist.info.yml" ]; then
  echo "*** Error: Script must be run from the base of the TTS Base installation profile."
  exit 1;
fi

if [ ! -d ${CONFIG_DIR} ]; then
  echo "*** Error: Config directory should be located here: ../../../config/sync/"
  exit 1;
fi

# Remove the old config.
rm ./config/install/*;
rm ./config/optional/*;
rm ./config/post-install/*;

# Copy over the site's config.
cp ${CONFIG_DIR}/* ./config/install/;

# Move some config to optional.
mv ./config/install/block.block.* ./config/optional/
mv ./config/install/responsive_image.styles.* ./config/optional/
mv ./config/install/image.style.max_* ./config/optional/
#mv ./config/install/core.entity_view_display.node.pet.default.yml ./config/optional/
#mv ./config/install/core.entity_view_display.node.pet.featured.yml ./config/optional/
#mv ./config/install/core.entity_view_display.node.pet.teaser.yml ./config/optional/
#mv ./config/install/core.entity_form_display.node.article.default.yml ./config/optional/
#mv ./config/install/core.entity_form_display.node.page.default.yml ./config/optional/
#mv ./config/install/core.entity_form_display.node.pet.default.yml ./config/optional/
#mv ./config/install/views.view.moderated_content.yml ./config/optional/

# Move workflow config to post-install.
mv ./config/install/workflows.workflow.editorial.yml ./config/post-install/

# Don't want the list of enabled modules.
rm ./config/install/core.extension.yml;
#
# Don't want devel settings.
rm ./config/install/devel.*.yml;
rm ./config/install/*.devel.yml;

# TTS Dependencies
function moveit {
  #echo "===> Processing $1"
  #ls -l $1
  mv $1 ./config/optional/
  #echo " "
  #read -p "..."
}
moveit "./config/install/basicshib.*"
moveit "./config/install/classy_paragraphs.*"
moveit "./config/install/core.entity_form_*"
moveit "./config/install/core.entity_view_*"
moveit "./config/install/crop.*"
moveit "./config/install/editor.editor.*"
moveit "./config/install/embed.button.*"
moveit "./config/install/entity_browser.browser.*"
moveit "./config/install/entityqueue.*"
moveit "./config/install/feeds.feed_type.*"
moveit "./config/install/field_collection.field_collection.*"
moveit "./config/install/field.field.*"
moveit "./config/install/field.storage.*"
moveit "./config/install/file_entity.type.*"
moveit "./config/install/filter.format.*"
moveit "./config/install/image.*"
moveit "./config/install/imce.*"
moveit "./config/install/isotope.*"
moveit "./config/install/media.type.*"
moveit "./config/install/menu_position.*"
moveit "./config/install/metatag.metatag_defaults.*"
moveit "./config/install/paragraphs.paragraphs_type.*"
moveit "./config/install/pathauto.pattern.*"
moveit "./config/install/rabbit_hole.behavior_settings.*"
moveit "./config/install/search_api.*"
moveit "./config/install/slick.optionset.*"
moveit "./config/install/system.action.*"
moveit "./config/install/tts_*"
moveit "./config/install/user.role.site_manager.*"
moveit "./config/install/views.view.*"
moveit "./config/install/migrate_plus.*"

#
# Remove UUIDs, etc.
# For use on Macs:
#find ./config/ -type f -exec sed -i '' '/^uuid: /d' {} \;
#find ./config/ -type f -exec sed -i '' '/_core/{N;d;}' {} \;
# For use on Linux:
find ./config/ -type f -exec sed -i -e '/^uuid: /d' {} \;
find ./config/ -type f -exec sed -i -e '/_core:/,+1d' {} \;

##rm ./config/install/image.style.*
##cp ${CONFIG_DIR}/image.style.* ./config/optional/;
##read -p "..."

# After Install
#had to change the front page from /node/16 to /node
#had to run the shibboleth patch script
sed -i 's/node\/16/node/g' ./config/install/system.site.yml
#/usr/local/etc/shibboleth-htaccess-patch.sh /var/www/drupal6

#echo "NOTE: you will need to do a 'drush cr' after the install and grant yourself the administrator role
#
#drush user-add-role administrator dp75@duke.edu
#
#"
