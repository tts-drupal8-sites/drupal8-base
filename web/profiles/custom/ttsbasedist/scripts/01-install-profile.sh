#!/bin/bash

mydir=''
drupalroot="/srv/www/drupal"
webroot="$drupalroot/web"
profilename="ttsbasedist"
profileroot="$webroot/profiles/custom/$profilename"
date_time=`date +"%Y%m%d-%HH%MM"`
rslt=""

function backitup {
  # backup current install profile
  rsync -av $profileroot/ ~/$profilename-$date_time/
}

function removeit {
  if [ -f $1 ]
  then
    rm -f $1
  fi
}

function copyit {
  # clone repo
  cd ~
  if [ -d d8-ref-dev_trinity_duke_edu ]
  then
    rm -rf d8-ref-dev_trinity_duke_edu
  fi
  git clone git@gitlab.oit.duke.edu:tts-drupal8-sites/d8-ref-dev_trinity_duke_edu.git
  git checkout dev

  # remove migrate configs
  cd d8-ref-dev_trinity_duke_edu
  removeit config/sync/migrate_drupal.settings.yml
  #removeit config/sync/migrate_plus*
  removeit config/sync/tts_migrate_duke_sources.settings.yml
  
  cd ~
  if [ -d config/sync ]
  then
    rm -rf config/sync
  fi
  mkdir -p config/sync
  rsync -av --delete d8-ref-dev_trinity_duke_edu/config/sync/ ~/config/sync
}

function moveit_optional {
  mv $1 $mydir/config/optional/
}

function moveit_tmp {
  mv $1 $mydir/config/tmp/
}

function moveit_tmp2 {
  mv $1 $mydir/config/tmp2/
}

function updateit {
  # Remove the old config.
  rm $mydir/config/install/*;
  rm $mydir/config/optional/*;
  rm $mydir/config/tmp/*;
  rm $mydir/config/tmp2/*;
  #rm $mydir/config/post-install/*;
  
  # Copy over the site's config.
  cp ~/config/sync/* $mydir/config/install/;
  
  # import after install profile

  moveit_tmp2 "$mydir/config/install/basicshib.*"
  #moveit_tmp2 "$mydir/config/install/block.block.tts_labs_ctrs_shibbolethlogin.yml"
  #moveit_tmp2 "$mydir/config/install/block.block.tts_sub_shibbolethlogin.yml"
  rm -f $mydir/config/install/block.block.tts_labs_ctrs_shibbolethlogin.yml
  rm -f $mydir/config/install/block.block.tts_sub_shibbolethlogin.yml

  moveit_tmp "$mydir/config/install/file_entity.settings.yml"
  moveit_tmp "$mydir/config/install/media_entity_soundcloud.settings.yml"
  moveit_tmp "$mydir/config/install/redirect.settings.yml"
  moveit_tmp "$mydir/config/install/redirect_404.settings.yml"
  moveit_tmp "$mydir/config/install/image.style.media_entity_browser_thumbnail.yml"
  moveit_tmp "$mydir/config/install/system.action.redirect_delete_action.yml"
  moveit_tmp "$mydir/config/install/views.view.media_entity_browser.yml"
  moveit_tmp "$mydir/config/install/views.view.redirect.yml"
  moveit_tmp "$mydir/config/install/views.view.redirect_404.yml"
  moveit_tmp "$mydir/config/install/core.entity_form_display.node.article.default.yml"
  moveit_tmp "$mydir/config/install/core.entity_form_display.node.course.default.yml"
  moveit_tmp "$mydir/config/install/core.entity_form_display.node.research_area.default.yml"
  moveit_tmp "$mydir/config/install/core.entity_form_display.paragraph.photo_gallery.default.yml"
  moveit_tmp "$mydir/config/install/core.entity_form_display.paragraph.video_gallery.default.yml"
  moveit_tmp "$mydir/config/install/core.entity_form_display.paragraph.video_gallery_two_column.default.yml"
  moveit_tmp "$mydir/config/install/embed.button.embed_audio.yml"
  moveit_tmp "$mydir/config/install/embed.button.embed_file.yml"
  moveit_tmp "$mydir/config/install/embed.button.embed_image.yml"
  moveit_tmp "$mydir/config/install/embed.button.embed_video.yml"
  moveit_tmp "$mydir/config/install/entity_browser.browser.audio_entity_browser.yml"
  moveit_tmp "$mydir/config/install/entity_browser.browser.file_entity_browser.yml"
  moveit_tmp "$mydir/config/install/entity_browser.browser.image_entity_browser.yml"
  moveit_tmp "$mydir/config/install/entity_browser.browser.media_browser.yml"
  moveit_tmp "$mydir/config/install/entity_browser.browser.modal_image_browser.yml"
  moveit_tmp "$mydir/config/install/entity_browser.browser.modal_video_browser.yml"
  moveit_tmp "$mydir/config/install/entity_browser.browser.video_entity_browser.yml"

  # Jay Beaton, dependencies
  moveit_optional "$mydir/config/install/block.block.*"

  # TCAS, depentencies
  moveit_optional "$mydir/config/install/core.entity_form_*"
  moveit_optional "$mydir/config/install/core.entity_view_*"
  moveit_optional "$mydir/config/install/crop.*"
  moveit_optional "$mydir/config/install/editor.editor.*"
  moveit_optional "$mydir/config/install/embed.button.*"
  moveit_optional "$mydir/config/install/entity_browser.browser.*"
  moveit_optional "$mydir/config/install/entityqueue.*"
  moveit_optional "$mydir/config/install/feeds.feed_type.*"
  moveit_optional "$mydir/config/install/field.field.*"
  moveit_optional "$mydir/config/install/field.storage.*"
  moveit_optional "$mydir/config/install/file_entity.type.*"
  moveit_optional "$mydir/config/install/filter.format.*"
  moveit_optional "$mydir/config/install/image.*"
  moveit_optional "$mydir/config/install/media.type.*"
  moveit_optional "$mydir/config/install/menu_position.*"
  moveit_optional "$mydir/config/install/metatag.metatag_defaults.*"
  moveit_optional "$mydir/config/install/migrate_plus.migration*"
  moveit_optional "$mydir/config/install/paragraphs.paragraphs_type.*"
  moveit_optional "$mydir/config/install/pathauto.pattern.*"
  moveit_optional "$mydir/config/install/rabbit_hole.behavior_settings.*"
  moveit_optional "$mydir/config/install/slick.optionset.*"
  moveit_optional "$mydir/config/install/system.action.*"
  moveit_optional "$mydir/config/install/tts_*"
  moveit_optional "$mydir/config/install/user.role.*"
  moveit_optional "$mydir/config/install/views.view.*"
  
  # Move workflow config to post-install.
  #mv $mydir/config/install/workflows.workflow.editorial.yml $mydir/config/post-install/
  
  # Don't want the list of enabled modules.
  rm $mydir/config/install/core.extension.yml;
  #
  # Don't want devel settings.
  rm $mydir/config/install/devel.*.yml;
  rm $mydir/config/install/*.devel.yml;
  
  #
  # Remove UUIDs, etc.
  # For use on Macs:
  #find $mydir/config/ -type f -exec sed -i '' '/^uuid: /d' {} \;
  #find $mydir/config/ -type f -exec sed -i '' '/_core/{N;d;}' {} \;
  # For use on Linux:
  find $mydir/config/ -type f -exec sed -i -e '/^uuid: /d' {} \;
  find $mydir/config/ -type f -exec sed -i -e '/_core:/,+1d' {} \;
  
  # After Install
  #had to change the front page from /node/16 to /node
  #had to run the shibboleth patch script
  sed -i 's/node\/16/node/g' $mydir/config/install/system.site.yml
}

function defaultimage {
  sed -i '/default_image:/,+5 d' $mydir/config/optional/field.field.block_content.footer_block.field_footer_logo.yml
}

function prompt {
  read -p "$1
" rslt
  if [ "$rslt" == "y" ]
  then
    $2
  fi
}

if [ ! -f TTSBaseDist.info.yml ]
then
  echo "==> Script must be run from the profile directory 'ttsbasedist'"
  exit
fi
mydir=`pwd`
prompt "==> backup current install profile? (y/n)" backitup
prompt "==> copy profile from d8-ref-dev? (y/n)" copyit
prompt "==> process config dependencies? (y/n)" updateit
prompt "==> remove default image? (y/n)" defaultimage
