# Media Entity Soundcloud
Soundcloud integration for Drupal.

## Installation

1. Enable the media and media_entity_soundcloud module.
2. Go to `/admin/structure/media` and click 'Add media type' to create a new
   bundle.
3. Under **Media source** select Soundcloud.
4. Save the bundle.

## Configuration

1. Go to the **Manage display** section for the media bundle.
2. For the source field selected, select **Soundcloud embed** under **Format**.
3. Click on the settings icon to configure the embedded player.
4. Save.

## Player configuration
1. Type: Visual or Classic
2. Options: Autoplay, Hide related, Show comments, Show user, Show reposts.
3. Width and height

## License

http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
