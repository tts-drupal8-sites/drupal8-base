<?php

declare(strict_types=1);

namespace Drupal\Tests\imagecache_external\Kernel;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Render\RenderContext;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\image\Entity\ImageStyle;
use Drupal\KernelTests\KernelTestBase;
use Drupal\responsive_image\Entity\ResponsiveImageStyle;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * Tests for Imagecache External.
 *
 * @coversDefaultClass \Drupal\imagecache_external\Plugin\Field\FieldFormatter\ImagecacheExternalResponsiveImage
 *
 * @group imagecache_external
 */
class ImagecacheExternalResponsiveImageFormatterTest extends KernelTestBase {

  /**
   * The test entity.
   *
   * @var \Drupal\entity_test\Entity\EntityTest
   */
  protected $entity;

  /**
   * Mock client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $mockClient;

  /**
   * The field name of the test field.
   *
   * @var string
   */
  protected $fieldName;

  /**
   * A view display of the test entity.
   *
   * @var \Drupal\Core\Entity\Entity\EntityViewDisplay
   */
  protected $display;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'breakpoint',
    'entity_test',
    'field',
    'file',
    'image',
    'imagecache_external',
    'responsive_image',
    'system',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $entity_type = 'entity_test';
    $this->installConfig([
      'system',
      'field',
      'image',
      'responsive_image',
      'imagecache_external',
    ]);
    $this->installEntitySchema($entity_type);
    $this->fieldName = $this->randomMachineName();

    $field_storage = FieldStorageConfig::create([
      'field_name' => $this->fieldName,
      'entity_type' => $entity_type,
      'type' => 'string',
    ]);
    $field_storage->save();

    $instance = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => $entity_type,
      'label' => $this->randomMachineName(),
    ]);
    $instance->save();

    $this->display = EntityViewDisplay::create([
      'targetEntityType' => $entity_type,
      'bundle' => $entity_type,
      'mode' => 'default',
      'status' => TRUE,
    ]);
    $this->display->save();

    $this->entity = EntityTest::create([
      'type' => 'entity_test',
      'name' => $this->randomMachineName(),
      $this->fieldName => 'https://imagecache-external.example.com/image/drupal.png',
    ]);
    $this->entity->save();

    $this->createImageStyle('foo');
    $this->createImageStyle('bar', 600, 600);

    // Add a responsive image style.
    $responsive_image_step = ResponsiveImageStyle::create([
      'id' => 'responsive_image',
      'label' => 'Responsive image',
      'breakpoint_group' => 'responsive_image.viewport_sizing',
      'fallback_image_style' => 'foo',
    ])->addImageStyleMapping(
      'test_breakpoint',
      '1x',
      [
        'image_mapping_type' => 'sizes',
        'image_mapping' => [
          'sizes' => '(min-width:700px) 700px, 100vw',
          'sizes_image_styles' => [
            'foo' => 'foo',
            'bar' => 'bar',
          ],
        ],
      ]
    );
    $responsive_image_step->save();
  }

  /**
   * @covers ::viewElements
   * @dataProvider providerLinkSetting
   */
  public function testFormatter(string $link, string $expected_uri): void {
    $this->display
      ->setComponent(
        $this->fieldName,
        [
          'type' => 'imagecache_external_responsive_image',
          'label' => 'hidden',
          'settings' => [
            'imagecache_external_responsive_style' => 'responsive_image',
            'imagecache_external_link' => $link,
          ],
        ]
      )
      ->save();

    $this->normalizeTestUrl($expected_uri);

    $renderer = $this->container->get('renderer');
    $this->mockClient(
      new Response(
        200,
        [],
        file_get_contents(dirname(__DIR__, 2) . '/assets/el-blue.png')
      ),
    );
    $rendered_output = $renderer->executeInRenderContext(
      new RenderContext(),
      function () use ($renderer) {
        $build = $this->entity
          ->get($this->fieldName)
          ->view($this->display->getMode());
        return $renderer->render($build);
      }
    );
    $dom = Html::load($rendered_output);
    // We should have an <img> and a <picture> element, and if the field is
    // configured to link the items, then a link also should appear.
    $this->assertCount(1, $dom->getElementsByTagName('img'));
    $this->assertCount(1, $dom->getElementsByTagName('picture'));
    $this->assertCount(
      $expected_uri ? 1 : 0,
      $links = $dom->getElementsByTagName('a'),
      'Responsive image is not wrapped into a link'
    );
    if ($expected_uri) {
      $this->assertEquals(
        $expected_uri,
        urldecode(
          $links->item(0)
            ->attributes
            ->getNamedItem('href')
            ->nodeValue
        )
      );
    }
  }

  /**
   * Data provider for ::testFormatter.
   *
   * @return string[][]
   *   The test cases.
   */
  public static function providerLinkSetting(): array {
    return [
      'Field not using link' => [
        'link setting' => '',
        'expected URL' => '',
      ],
      'Linked to file' => [
        'link setting' => 'file',
        'expected URL' => 'public://externals/0957f4609929b129757304fd836683f4.png',
      ],
      'Linked to content' => [
        'link setting' => 'content',
        'expected URL' => '/entity_test/1',
      ],
    ];
  }

  /**
   * Normalizes test URLs.
   *
   * Replaces the scheme in URLs with the scheme's relative path if the scheme
   * has its own stream wrapper in Drupal.
   *
   * @param string $url
   *   The URL to normalize.
   */
  protected function normalizeTestUrl(string &$url): void {
    $scheme = parse_url($url)['scheme'] ?? NULL;
    if (
      $scheme &&
      $wrapper = $this->container->get('stream_wrapper_manager')->getViaScheme($scheme)
    ) {
      $wrapper_uri = $wrapper->getUri();
      $url = '/' . ltrim(
        preg_replace(
          "#^$wrapper_uri#",
          $wrapper->realpath(),
          $url
        ),
        '/'
      );
    }
  }

  /**
   * Creates an image style with a single crop effect.
   *
   * @param string $id
   *   The ID of the image style. Also used as label.
   * @param int $width
   *   The crop width. Optional, defaults to 400.
   * @param int $height
   *   The crop height. Optional, defaults to 400.
   */
  protected function createImageStyle(string $id, int $width = 400, int $height = 400): void {
    $image_stype = ImageStyle::create(['name' => $id, 'label' => $id]);
    $image_stype->addImageEffect([
      'id' => 'image_scale',
      'data' => [
        'upscale' => TRUE,
        'width' => $width,
        'height' => $height,
      ],
      'weight' => 0,
    ]);
    $image_stype->save();
  }

  /**
   * Replaces the HTTP client service with a mock returning the given responses.
   *
   * @param \GuzzleHttp\Psr7\Response ...$responses
   *   List of responses to return.
   */
  protected function mockClient(Response ...$responses): void {
    if (!isset($this->mockClient)) {
      // Create a mock and queue responses.
      $mock = new MockHandler($responses);
      $handler_stack = HandlerStack::create($mock);
      $this->mockClient = new Client(['handler' => $handler_stack]);
    }
    $this->container->set('http_client', $this->mockClient);
  }

}
