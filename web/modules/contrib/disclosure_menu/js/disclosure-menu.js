const ONLY_OPEN = 'only_open';
const KEYBOARD_ONLY = 'keyboard_only';

(function (Drupal, once, drupalSettings) {
  'use strict';

  function toggleMenu(toggle, menu) {
    // Set whether the toggle is expanded.
    if (toggle.getAttribute('aria-expanded') === 'false') {
      toggle.setAttribute('aria-expanded', 'true');
    } else {
      toggle.setAttribute('aria-expanded', 'false');
    }
    // Toggle the menu open or closed.
    menu.classList.toggle('hide');
    menu.style.display = menu.style.display == 'none' ? '' : 'none';
  }

  function hideMenu(toggle, menu) {
    // Set the toggle to not expanded.
    toggle.setAttribute('aria-expanded', 'false');
    // Hide the menu
    menu.classList.add('hide');
    menu.style.display = 'none';
  }

  function showMenu(toggle, menu) {
    // Set the toggle to expanded.
    toggle.setAttribute('aria-expanded', 'true');
    // Show the menu
    menu.classList.remove('hide');
    menu.style.display = '';
  }

  function configureToggleButton(toggle, menuSettings = null) {
    const menuID = toggle.getAttribute('aria-controls');
    const menu = document.getElementById(menuID);
    // Hide button if there's no corresponding menu.
    if (!menu) {
      toggle.style.display = 'none';
      return;
    }
    if (menuSettings && menuSettings['resolveHoverClick'] == ONLY_OPEN) {
      // On button click, open the menu.
      toggle.addEventListener('click', () => showMenu(toggle, menu));
    } else {
      // On button click, toggle the menu.
      toggle.addEventListener('click', (event) => {
        if (
          menuSettings &&
          menuSettings['resolveHoverClick'] == KEYBOARD_ONLY
        ) {
          // Event detail will only be 0 on keyboard input.
          if (event.detail == 0) {
            toggleMenu(toggle, menu);
          }
        } else {
          toggleMenu(toggle, menu);
        }
      });
    }

    // Close menu after tabbing past it.
    menu.addEventListener('focusout', (event) => {
      if (
        !menu.contains(event.relatedTarget) &&
        event.relatedTarget !== toggle
      ) {
        // This is a workaround to simulate the blur event triggering after the
        // click event. See https://dgo.to/3491739#comment-15924663
        setTimeout(() => hideMenu(toggle, menu), 0);
      }
    });
    toggle.addEventListener('blur', (event) => {
      if (!menu.contains(event.relatedTarget)) {
        // This is a workaround to simulate the blur event triggering after the
        // click event. See https://dgo.to/3491739#comment-15924663
        setTimeout(() => hideMenu(toggle, menu), 0);
      }
    });
  }

  // Keyboard Navigation.
  function disclosureMenuNav(id, menuSettings, context) {
    once(
      'js-toggle-init',
      `#${id} button.menu__submenu-toggle`,
      context
    ).forEach((toggle) => configureToggleButton(toggle, menuSettings));
  }

  // Hover Navigation.
  function disclosureMenuHoverNav(id, menuSettings, context) {
    // Set timeout to hide submenu.
    function startHideIntent(menu, toggle) {
      const hideIntent = setTimeout(
        () => hideMenu(toggle, menu),
        menuSettings['hoverHideDelay']
      );
      menu.setAttribute('data-hide-intent', hideIntent);
    }

    // Remove timeout to hide submenu, if it exists.
    function endHideIntent(menu) {
      const hideIntent = menu.getAttribute('data-hide-intent');
      if (hideIntent) {
        clearTimeout(hideIntent);
        menu.removeAttribute('data-hide-intent');
      }
    }

    // Set timeout to show submenu.
    function startShowIntent(menu, toggle) {
      const showIntent = setTimeout(
        () => showMenu(toggle, menu),
        menuSettings['hoverShowDelay']
      );
      menu.setAttribute('data-show-intent', showIntent);
    }

    // Remove timeout to show submenu, if it exists.
    function endShowIntent(menu) {
      const showIntent = menu.getAttribute('data-show-intent');
      if (showIntent) {
        clearTimeout(showIntent);
        menu.removeAttribute('data-show-intent');
      }
    }

    once('js-hover-init', `#${id} li[data-submenu-id]`, context).forEach(
      (li) => {
        const submenuID = li.getAttribute('data-submenu-id');
        const submenu = document.getElementById(submenuID);
        const toggle = document.querySelector(
          `#${id} button[aria-controls="${submenuID}"]`
        );
        if (!submenu || !toggle) return;

        // When hover over parent li, show submenu.
        li.addEventListener('pointerover', () => {
          endHideIntent(submenu);
          startShowIntent(submenu, toggle);
        });

        // When not hovering over parent li, hide submenu.
        li.addEventListener('pointerout', () => {
          endShowIntent(submenu);
          startHideIntent(submenu, toggle);
        });
      }
    );
  }

  Drupal.behaviors.disclosure_menu = {
    attach: function (context, settings) {
      once(
        'js-menu-disclosure-init',
        'nav > button.menu-toggle[aria-controls]',
        context
      ).forEach((menuToggle) => configureToggleButton(menuToggle));

      // Iterate through disclosure menus and apply navigation settings.
      for (const id in drupalSettings.disclosureMenu) {
        if (Object.hasOwnProperty.call(drupalSettings.disclosureMenu, id)) {
          const menuSettings = drupalSettings.disclosureMenu[id];
          disclosureMenuNav(id, menuSettings, context);
          if (menuSettings.hover) {
            disclosureMenuHoverNav(id, menuSettings, context);
          }
        }
      }
    },
    detach: function (context, settings) {},
  };
})(Drupal, once, drupalSettings);
