# Disclosure Menu

This module provides an alternative to Drupal's default menu block that uses disclosure buttons for submenus. These are buttons next to menu links that toggle open the respective submenu. This allows keyboard users to choose whether they want to navigate into a submenu instead of being forced to tab through every single link in a multi-level menu. These buttons come complete with ARIA labels to inform users what the buttons control and what their status is.


## Requirements

This module requires the following modules:
- [Token](https://www.drupal.org/project/token)
- [Twig Tweak](https://www.drupal.org/project/twig_tweak)


## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the Disclosure Menu module at Administration > Extend.
2. Add and configure a menu block from the "Disclosure Menu" category to your site.
