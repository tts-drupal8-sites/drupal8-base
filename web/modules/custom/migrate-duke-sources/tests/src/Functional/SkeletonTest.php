<?php

namespace Drupal\Tests\tts_migrate_duke_sources\Functional;

use Drupal\Tests\BrowserTestBase;

/*
 * Make a skeleton test
 *
 * @group tts_migrate_duke_sources
 */

class SkeletonTest extends BrowserTestBase {

	// a list of modules to load which are required to run the test
	public static $modules = [
		'user',
		'tts_migrate_duke_sources',
	];

	// this runs before the group tests
	protected function setUp() {
		parent::setUp();
	}

}
