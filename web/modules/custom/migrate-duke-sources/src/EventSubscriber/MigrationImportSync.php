<?php

namespace Drupal\tts_migrate_duke_sources\EventSubscriber;

use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Event\MigrateImportEvent;
use Drupal\migrate\Event\MigrateRollbackEvent;
use Drupal\migrate\Event\MigrateRowDeleteEvent;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_plus\Event\MigrateEvents as MigratePlusEvents;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MigrationImportSync implements EventSubscriberInterface
{
    protected $dispatcher;

    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public static function getSubscribedEvents()
    {
        $events = [];
        $events[MigrateEvents::PRE_IMPORT][] = ['sync'];

        return $events;
    }

    public function sync(MigrateImportEvent $event)
    {
        $migration = $event->getMigration();
        if (!empty($migration->syncSource)) {
            $id_map = $migration->getIdMap();
            $id_map->prepareUpdate();
            $source = $migration->getSourcePlugin();
            $source->rewind();
            $source_id_values = [];
            while ($source->valid()) {
                $source_id_values[] = $source->current()->getSourceIdValues();
                $source->next();
            }
            $id_map->rewind();
            $destination = $migration->getDestinationPlugin();
            while ($id_map->valid()) {
                $map_source_id = $id_map->currentSource();
                if (!in_array($map_source_id, $source_id_values, true)) {
                    // // DUKE CODE

                    // // this is slightly expiremental.  future work will probably include pivoting on $map_source_id
                    // // and it would be good to abstract so that migrations can provide this as a configuration value

                    // leave this
                    $destination_ids = $id_map->currentDestination();
                    $this->dispatchRowDeleteEvent(MigrateEvents::PRE_ROW_DELETE, $migration, $destination_ids);
                    $this->dispatchRowDeleteEvent(MigratePlusEvents::MISSING_SOURCE_ITEM, $migration, $destination_ids);

                    // // by commenting out here we prevent the normal rollback aka delete of our nids
                    // $destination->rollback($destination_ids);

                    // unpublish the nodes
                    $nodes = \Drupal\node\Entity\Node::loadMultiple($destination_ids);
                    foreach ($nodes as $node) {
                        $node->set('status', 0);
                        $node->save();
                    }

                    // leave this
                    $this->dispatchRowDeleteEvent(MigrateEvents::POST_ROW_DELETE, $migration, $destination_ids);

                    // // prevent the id from leaving the migration map.  this prevents duplication if this record comes back
                    // $id_map->delete($map_source_id);
                }
                $id_map->next();
            }
            $this->dispatcher->dispatch(new MigrateRollbackEvent($migration), MigrateEvents::POST_ROLLBACK);
        }
    }

    protected function dispatchRowDeleteEvent($event_name, MigrationInterface $migration, array $destination_ids)
    {
        // Symfony changing dispatcher so implementation could change.
        $this->dispatcher->dispatch(new MigrateRowDeleteEvent($migration, $destination_ids), $event_name);
    }
}
