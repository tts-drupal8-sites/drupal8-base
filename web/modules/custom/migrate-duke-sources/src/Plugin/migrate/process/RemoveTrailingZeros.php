<?php
/**
 * @file
 * Contains Drupal\tts_migrate_duke_sources\Plugin\migrate\process\RemoveTrailingZeros
 */

namespace Drupal\tts_migrate_duke_sources\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * @MigrateProcessPlugin(
 *   id = "remove_trailing_zeros"
 * )
 */
class RemoveTrailingZeros extends ProcessPluginBase
{
    public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property)
    {
        $_value = (float) $value;

        return $_value;
    }
}
