<?php
/**
 * @file
 * Contains Drupal\tts_migrate_duke_sources\Plugin\migrate\process\Title
 */

namespace Drupal\tts_migrate_duke_sources\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * @MigrateProcessPlugin(
 *   id = "title"
 * )
 */
class Title extends ProcessPluginBase
{
    public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property)
    {
        $title = strlen($title) > 250 ? substr($title, 0, 250) : $title;

        return $title;
    }
}
