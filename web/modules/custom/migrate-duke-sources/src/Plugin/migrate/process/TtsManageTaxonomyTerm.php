<?php
/**
 * @file
 * Contains Drupal\tts_migrate_duke_sources\Plugin\migrate\process\Title
 */

namespace Drupal\tts_migrate_duke_sources\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\taxonomy\Entity\Term;

// // dev code to remove all terms in the vocab
// $tids = \Drupal::entityQuery('taxonomy_term')
//     ->condition('vid', $vid)
//     ->execute();
// entity_delete_multiple('taxonomy_term', $tids);
// return null;

/**
 * @MigrateProcessPlugin(
 *   id = "tts_manage_taxonomy_term"
 * )
 */
class TtsManageTaxonomyTerm extends ProcessPluginBase
{
    /**
     * {@inheritdoc}
     */
    public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property)
    {
        // determine what vocabulary to use
        $config = \Drupal::config('tts_migrate_duke_sources.settings');
        $taxonomies = $config->get('taxonomy_by_field');
        $vid = $taxonomies[$destination_property];
        if ($destination_property == 'target_id') {
            $vid = $taxonomies[$this->configuration['source']['taxonomy_by_field']];

            // because of the added taxonomy_by_field property, we have to re-calibrate the $value
            $value = $value[0];
        }

        // no need to save empty values
        if (empty($value)) {
            return null;
        }

        // get existing terms in the system
        //$terms = taxonomy_term_load_multiple_by_name($value, $vid);
        $taxonomy = \Drupal::entityTypeManager()->getStorage("taxonomy_term");
        $terms = $taxonomy->loadByProperties(['name' => $value, 'vid' => $vid]);

        if (empty($terms)) {
            $tid = Term::create([
              'name' => $value,
              'vid' => $vid,
            ])->save();
        } else {
            $term = array_pop($terms);
            $tid = $term->id();
        }

        return $tid;
    }
}
