<?php
/**
 * @file
 * Contains Drupal\tts_migrate_duke_sources\Plugin\migrate\process\DateToTimestamp
 */

namespace Drupal\tts_migrate_duke_sources\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * @MigrateProcessPlugin(
 *   id = "date_to_timestamp"
 * )
 */
class DateToTimestamp extends ProcessPluginBase
{
    public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property)
    {
        if (!empty($value) && strtotime($value) > 0) {
            $timestamp = strtotime($value);

            return $timestamp;
        }

        return $value;
    }
}
