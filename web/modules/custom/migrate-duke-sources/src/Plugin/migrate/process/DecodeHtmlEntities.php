<?php
/**
 * @file
 * Contains Drupal\tts_migrate_duke_sources\Plugin\migrate\process\DecodeHTMLEntities
 */

namespace Drupal\tts_migrate_duke_sources\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * @MigrateProcessPlugin(
 *   id = "decode_html_entities"
 * )
 */
class DecodeHtmlEntities extends ProcessPluginBase 
{

  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    return htmlspecialchars_decode($value, ENT_QUOTES);
  }

}
