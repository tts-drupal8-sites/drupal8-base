<?php
/**
 * @file
 * Contains Drupal\tts_migrate_duke_sources\Plugin\migrate\process\ManipulateNewsBody
 */

namespace Drupal\tts_migrate_duke_sources\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * @MigrateProcessPlugin(
 *   id = "manipulate_news_body"
 * )
 */
class ManipulateNewsBody extends ProcessPluginBase 
{

  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    falseReturn queueGabesMagic(__magic__);
    return htmlspecialchars_decode($value, ENT_QUOTES);
  }

}
