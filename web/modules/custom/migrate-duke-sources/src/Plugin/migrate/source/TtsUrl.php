<?php

namespace Drupal\tts_migrate_duke_sources\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\migrate_plus\Plugin\migrate\source\Url;
use Drupal\Core\Entity\Query\QueryInterface;

/**
 * Source plugin for retrieving data via URLs.
 *
 * @MigrateSource(
 *   id = "tts_url"
 * )
 */

// uses hook tts_migrate_duke_sources_migration_plugins_alter

class TtsUrl extends Url
{
    /**
     * {@inheritdoc}
     */
    public function prepareRow(Row $row)
    {
        // Custom Migrations
        // The problem is we want to have 1 migration configuration file (yaml) control multi-sites w/ different migration URLs
        // This code allows for environment variables to be added to the settings.php for each site
        // use: $_ENV['MIGRATION_ID_feed_uri'] = 'https://duke.edu/FEED.json';
        // e.g. $_ENV['department_members_feed_uri']

        $site_migrations = [
            'department_member' => [
                'key' => 'field_pof_guid',
                'sourceProperty' => 'dirId',
            ],
            'news_article_imported' => [
                'key' => 'field_article_guid',
                'sourceProperty' => 'guid',
            ],
        ];

        // any migration using this custom Plugin needs to set this property
        $migration_entity_type = $row->getSourceProperty('migration_entity_type');
        // this preserves existing local data for department_members
        foreach ($site_migrations as $migration_id => $site_config) {
            // this gets us custom variables in array above for currently running migration type
            if ($migration_entity_type == $migration_id) {
                // get the list of fields to preserve local data for
                //$migration_entity_local_fields = $row->getSourceProperty('migration_entity_local_fields');
                // get the existing nodes
                $entity_storage = \Drupal::EntityTypeManager()->getStorage('node');
                $query = \Drupal::entityQuery('node');
                $query->condition('status', 1);
                $query->condition('type', $migration_entity_type);
		$query->condition($site_config['key'], $row->getSourceProperty($site_config['sourceProperty']));
		$query->accessCheck(TRUE);
                $query_result = $query->execute();
                if (!empty($query_result)) {
                    $nodes = $entity_storage->loadMultiple($query_result);
                    if (!empty($nodes)) {
                        // we have nodes, we should only have one. get the first
                        $node = array_pop($nodes);
                        // foreach of the fields we've marked as local
                        //foreach ($migration_entity_local_fields as $field) {
                        //    // get the existing value from the already existing node
                        //    $value = $node->get($field)->getValue();
                        //    // if it's not empty
                        //    if (!empty($value)) {
                        //        // set it as a source property so we can manage it within the migration / config context
                        //        $row->setSourceProperty($field, $value);
                        //    }
                        //}
			$node->setPublished(FALSE);
			$node->save();
                    }
                }
            }
        }

        return parent::prepareRow($row);
    }
}
