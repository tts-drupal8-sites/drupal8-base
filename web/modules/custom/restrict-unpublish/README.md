# Restrict Unpublish

A simple module that prevents Content Author, Site Editor, and Site Builder roles from unpublishing basic pages whose *Prevent Unpublish* box is checked.

## Authors and acknowledgment
Andy Smith (andy.smith@duke.edu)

## License
GPL-2.0 or later

