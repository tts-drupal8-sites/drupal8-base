<?php

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_alter().
 */
function restrict_unpublish_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  // Retrieve the current route match.
  $route_match = \Drupal::service('current_route_match');

  // Ensure we are on a node edit form.
  if ($route_match->getRouteName() !== 'entity.node.edit_form') {
    return;
  }

  // Retrieve the node entity from the route.
  $node = $route_match->getParameter('node');
  if (!$node || $node->getEntityTypeId() !== 'node' || $node->bundle() !== 'page') {
    return;
  }

  // Define roles and conditions.
  $restricted_roles = ['content_author', 'site_editor', 'site_builder'];
  $manage_roles = ['site_manager', 'administrator'];

  // Retrieve the current user.
  $account = \Drupal::currentUser();

  // If the user is a manager or admin, no restriction applies.
  if (array_intersect($manage_roles, $account->getRoles())) {
    return;
  }

  // Check the field_prevent_unpublish field.
  if ($node->hasField('field_prevent_unpublish') && $node->get('field_prevent_unpublish')->value) {
    // If the field is checked and the user has a restricted role, disable the status toggle.
    if (array_intersect($restricted_roles, $account->getRoles())) {
      $form['status']['#disabled'] = 'disabled';
    }
  }
}

