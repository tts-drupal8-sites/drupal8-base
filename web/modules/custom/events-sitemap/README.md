# Events Sitemap

Provides a valid XML sitemap by overriding the Views Data Export XML output for the Events Sitemap view.

## Authors and acknowledgment
Andy Smith (andy.smith@duke.edu)

## License
GPL-2.0 or later

