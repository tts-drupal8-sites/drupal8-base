<?php

namespace Drupal\events_sitemap\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class SitemapResponseSubscriber implements EventSubscriberInterface {

  public static function getSubscribedEvents() {
    return [
      KernelEvents::RESPONSE => ['onResponse', -10],
    ];
  }

  public function onResponse(ResponseEvent $event) {
    $request = $event->getRequest();
    $path = $request->getPathInfo();

    // Check if the request is for the sitemap XML view.
    if ($path === '/event_sitemap.xml') {
      $response = $event->getResponse();
      $content = $response->getContent();

      // Transform the XML content to the proper sitemap format.
      $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><urlset/>');
      $xml->addAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
      $xml->addAttribute('xmlns:xmlns:xhtml', 'http://www.w3.org/1999/xhtml');

      // Extract <item> elements from the original output.
      $old_xml = new \SimpleXMLElement($content);
      foreach ($old_xml->item as $item) {
        $url = $xml->addChild('url');
        $url->addChild('loc', (string) $item->loc);
        $url->addChild('lastmod', trim((string) $item->lastmod));
        $url->addChild('changefreq', (string) $item->changefreq);
        $url->addChild('priority', (string) $item->priority);
      }

      // Set the modified XML as the new response.
      $response->setContent($xml->asXML());
      $response->headers->set('Content-Type', 'application/xml; charset=UTF-8');
    }
  }
}

