# Disable Node Title

This is a simple module which disables the title field on the node edit form of content types within the TCA&S distribution whose nodes are created via an import (e.g. _Event - Imported_).

## Authors and acknowledgment
Andy Smith (andy.smith@duke.edu); hat tip to oknate (https://drupal.stackexchange.com/users/45409/oknate)

## License
GPL-2.0 or later

