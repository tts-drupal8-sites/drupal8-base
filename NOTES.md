- added phpunit.xml for local automated testing
- removed corrupted and duplicate .gitignore file from web/ directory
- removed duplicate .gitattributes in repo root, because of the drupal boilerplate version in the web/ directory
	-- 90% confident i've removed the correct one.  in any case, these are identical.  we won't need both
	-- https://www.drupal.org/project/drupal/issues/3084326
- added rule in .gitignore to prevent vendor submodules from being added to our repos as file references

- forced to add "vendor/bin/.phpunit/" to repo. this is why we should be building composer on the server, like with our symfony apps.  it's also made me realize that we can't properly leverage composer require-dev commands -- it all ends up in production



